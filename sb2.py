# -*- coding: utf-8 -*-
from linepy import *
from gtts import gTTS
from googletrans import Translator
import json, time, datetime, requests, urllib, subprocess

client = LineClient()
#client = LineClient(authToken='AuthToken')
client.log("Auth Token : " + str(client.authToken))
channel = LineChannel(client)
client.log("Channel Access Token : " + str(channel.channelAccessToken))

poll = LinePoll(client)
cctv = {
    "cyduk":{},
    "point":{},
    "sidermem":{}
}

mulai = time.time()

def waktu(secs):
    mins, secs = divmod(secs,60)
    hours, mins = divmod(mins,60)
    return '%02d Jam %02d Menit %02d Detik' % (hours, mins, secs)

def mention(to, nama):
    aa = ""
    bb = ""
    strt = int(19)
    akh = int(19)
    nm = nama
    myid = client.getProfile().mid
    if myid in nm:    
      nm.remove(myid)
    #print nm
    for mm in nm:
      akh = akh + 2
      aa += """{"S":"""+json.dumps(str(strt))+""","E":"""+json.dumps(str(akh))+""","M":"""+json.dumps(mm)+"},"""
      strt = strt + 6
      akh = akh + 4
      bb += "◎ @x \n"
    aa = (aa[:int(len(aa)-1)])
    text = "  「Mention All」 \n"+bb + "    「 Total: " + str(len(nm)) +  " Tag 」"
    try:
       client.sendMessage(to, text, contentMetadata={'MENTION':'{"MENTIONEES":['+aa+']}'}, contentType=0)
    except Exception as error:
       print(error)

while True:
    try:
        ops=poll.singleTrace(count=50)
        
        for op in ops:
            if op.type == OpType.SEND_MESSAGE:
                msg = op.message
                text = msg.text
                msg_id = msg.id
                receiver = msg.to
                sender = msg._from
                try:
                    if msg.contentType == 0:
                        if msg.toType == 2:
                            client.sendChatChecked(receiver, msg_id)
                            contact = client.getContact(sender)
                            if text.lower() == 'me':
                                client.sendMessage(receiver, None, contentMetadata={'mid': sender}, contentType=13)
                            if 'jam' in text.lower():
                                client.sendMessage(receiver, "Sekarang:\n" + datetime.datetime.today().strftime('%A, %d %B %Y  \n%d/%m/%Y \n%H:%M:%S') + " WIB")
                            if text.lower() == 'runtime':
                                eltime = time.time() - mulai
                                van = "System sudah berjalan selama: \n" + waktu(eltime)
                                client.sendMessage(receiver,van)
                            if text.lower() == 'speed':
                                start = time.time()
                                client.sendText(receiver, "...")
                                elapsed_time = time.time() - start
                                client.sendText(receiver, "%s detik" % (elapsed_time))
                            if text == 'txt ':
                                text = text.replace('txt ','')
                                client.kedapkedip(receiver, text)
                                print(kedapkedip)
                            if text.lower() == 'ListGroup':
                                gid = client.getGroupIdsJoined()
                                num = 1
                                msgs = "List My Groups\n"
                                for i in gid:
                                    ginfo = client.getGroup(i)
                                    msgs += "\n%i. %s" % (num, ginfo.name + "[" + str(len(ginfo.members)) + "]")
                                    num = (num+1)
                                    msgs += "\n\nTotal %i Group(s)" % len(gid)
                                    client.sendText(receiver, msgs)
                            if text.lower() == 'pict':
                                try:
                                    key = eval(msg.contentMetadata["MENTION"])
                                    u = key["MENTIONEES"][0]["M"]
                                    a = client.getContact(u).pictureStatus
                                    print(client.getContact(u))
                                    client.sendImageWithURL(receiver, 'http://dl.profile.line.naver.jp/'+a)
                                except Exception as e:
                                    print(e)
                            if text.lower() == 'cover':
                                try:
                                    key = eval(msg.contentMetadata["MENTION"])
                                    u = key["MENTIONEES"][0]["M"]
                                    a = client.getProfileCoverURL(mid=u)
                                    print(a)
                                    client.sendImageWithURL(receiver, a)
                                except Exception as e:
                                    print(e)
                            if text == 'Gn ':
                                a = client.getGroup(msg.to)
                                a.name = text.replace('Gn ','')
                                client.updateGroup(a)
                            if text == 'Cn':
                                a = client.getProfile()
                                a.displayName = text.replace('Cn ','')
                                client.updateProfile(a)
                            if 'tr-' in text.lower():
                                cond = text.lower().split('-')
                                bhs = cond[1]
                                isi = cond[2]
                                translator = Translator()
                                hasil = translator.translate(isi, dest=bhs)
                                A = hasil.text
                                tts = gTTS(text=isi, lang=bhs)
                                tts.save("hasil.mp3")
                                A = A.encode('utf-8')
                                client.sendText(msg.to, A)
                                client.sendAudio(receiver, "hasil.mp3")
                            if text.lower() == 'apakah':
                                Q = text.lower().replace('apakah','')
                                A = ('iya','tidak')
                                A2 = random.choice(A)
                                tts = gTTS(text=A2, lang='id')
                                tts.save('tts.mp3')
                                client.sendAudio(receiver, 'tts.mp3')
                            if text.lower() == 'sms-':
                                space = text.lower().split('-')
                                nomor = space[1]
                                pesan = space[2]
                                s = requests.get('http://aksamedia.com/googlex/sms_api_xwm.php?kirimsms=a&nomor='+urllib.urlencode(nomor)+'&message='+urllib.urlencode(pesan))
                                data = json.loads(s.text)
                                status = "「Progress」"
                                    if data["message"] == "Done !":
                                        status += "\nMessage : Terkirim"
                                    else:
                                        status += "\nMessage : " + str(data["message"])
                                    if data["err"] == False:
                                        status += "\nError : Gk ada"
                                    else:
                                        status += "\nError : " + str(data["err"])
                                    if data["success"] == True:
                                        status += "\nSuccess : Terkirim"
                                    else:
                                        status += "\nSuccess : " + str(data["success"])
                                    cl.sendText(msg.to,status)
                            if '.music ' in text.lower():
                                songname = text.lower().replace('.music ','')
                                params = {'songname':song}
                                r = requests.get('https://ide.fdlrcn.com/workspace/yumi-apis/joox?'+urllib.urlencode(params))
                                data = r.text
                                data = data.encode('utf-8')
                                data = json.loads(data)
                                for song in data:
                                    client.sendText(to,"This Your Music Request.\n\nJudul: " + song[0] + "\nWaktu: " + song[1] + "\nLink: " + song[3] + "\nDownload: " + song[4])
                                    client.sendAudioWithURL(to, song[3])
                            if '.lyric ' in text.lower():
                                songname = text.lower().replace('.lyric ','')
                                params = {'songname':song}
                                r = requests.get('https://ide.fdlrcn.com/workspace/yumi-apis/joox?'+urllib.urlencode(params))
                                data = r.text
                                data = data.encode('utf-8')
                                data = json.loads(data)
                                for song in data:
                                    client.sendText(to,"Lyrics Of " + song[0] + ":\n\n"+ song[5])
                            if 'Cek ' in text.lower():
                                key = eval(msg.contentMetadata["MENTION"])
                                key1 = key["MENTIONEES"][0]["M"]
                                mi = client.getContact(key1)
                                client.sendText(msg.to,"Mid: \n" + key1)
                            if 'mid @' in text.lower():
                                _name = text.lower().replace('mid @','')
                                _nametarget = _name.rstrip(' ')
                                gs = client.getGroup(msg.to)
                                for g in gs.members:
                                    if _nametarget == g.displayName:
                                        client.sendText(msg.to, g.mid)
                                    else:
                                        pass
                            if 'info @' in text.lower():
                                nama = text.lower().replace('info @','')
                                target = nama.rstrip(' ')
                                van = client.getGroup(msg.to)
                                for info in van.members:
                                    if target == info.displayName:
                                        mid = client.getContact(info.mid)
                                        try:
                                            cover = client.getProfileCoverURL(mid=info)
                                        except:
                                            cover = ''
                                        client.sendText(receiver, '[Display Name]:\n' + mid.displayName + '\n\n[Mid]:\n' + info.mid + '\n\n[Bio]:\n' + mid.statusMessage + '\n\n[Ava]:\nhttp://dl.profile.line.naver.jp/' + mid.pictureStatus + '\n\n[Cover]:\n' + str(cover))
                                    else:
                                        pass
                            if text.lower() == 'tagall':
                                group = client.getGroup(msg.to)
                                nama = [contact.mid for contact in group.members]
                                nm1, nm2, nm3, nm4, nm5, nm6, jml = [], [], [], [], [], [], len(nama)
                                if jml <= 100:
                                    mention(msg.to, nama)
                                if jml > 100 and jml < 600:
                                    for i in range(0, 99):
                                        nm1 += [nama[i]]
                                    mention(msg.to, nm1)
                                    for j in range(100, len(nama)-1):
                                        nm2 += [nama[j]]
                                    mention(msg.to, nm2)
                                    for k in range(200, len(nama)-1):
                                        nm3 += [nama[k]]
                                    mention(msg.to, nm3)
                                    for l in range(300, len(nama)-1):
                                        nm4 += [nama[l]]
                                    mention(msg.to, nm4)
                                    for m in range(400, len(nama)-1):
                                        nm5 += [nama[m]]
                                    mention(msg.to, nm5)
                                    for n in range(500, len(nama)-1):
                                        nm6 += [nama[n]]
                                    mention(msg.to, nm6)
                                if jml > 600:
                                    print("Waduh 600 member")
                                    client.sendText('Mentioned: ' + str(jml))
                            if text.lower() == 'ceksider':
                                try:
                                    del cctv['point'][msg.to]
                                    del cctv['sidermem'][msg.to]
                                    del cctv['cyduk'][msg.to]
                                except:
                                    pass
                                cctv['point'][msg.to] = msg.id
                                cctv['sidermem'][msg.to] = ""
                                cctv['cyduk'][msg.to]=True
                            if text.lower() == 'offread':
                                if msg.to in cctv['point']:
                                    cctv['cyduk'][msg.to]=False
                                    client.sendText(msg.to, cctv['sidermem'][msg.to])
                                else:
                                    client.sendText(msg.to, "Heh belom di Set")
                            if text.lower() == 'ifconfig':
                                botKernel = subprocess.Popen(["ifconfig"], stdout=subprocess.PIPE).communicate()[0]
                                client.sendText(receiver, botKernel + "\n\n====SERVER INFO====")
                            if text.lower() == 'system':
                                botKernel = subprocess.Popen(["df","-h"], stdout=subprocess.PIPE).communicate()[0]
                                client.sendText(receiver, botKernel + "\n\n====SYSTEM INFO====")
                except Exception as e:
                    client.log("[SEND_MESSAGE] ERROR : " + str(e))

            if op.type == OpType.NOTIFIED_READ_MESSAGE:
                try:
                    if cctv['cyduk'][op.param1]==True:
                        if op.param1 in cctv['point']:
                            Name = client.getContact(op.param2).displayName
                            if Name in cctv['sidermem'][op.param1]:
                                pass
                            else:
                                cctv['sidermem'][op.param1] += "Kang Sider \n・" + Name
                                #cl.mention(op.param1, op.param2)
                                if " " in Name:
                                    nick = Name.split(' ')
                                    if len(nick) == 2:
                                        client.sendText(op.param1, 'haii... '+nick[0])
                                    else:
                                        client.sendText(op.param1, 'Hello '+nick[1])
                                else:
                                    client.sendText(op.param1, 'Eh '+Name)
                        else:
                            pass
                    else:
                        pass
                except:
                    pass

            else:
                pass

            # Don't remove this line, if you wan't get error soon!
            poll.setRevision(op.revision)
            
    except Exception as e:
        client.log("[SINGLE_TRACE] ERROR : " + str(e))
